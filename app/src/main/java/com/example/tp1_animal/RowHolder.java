package com.example.tp1_animal;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

public class RowHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener {
    TextView nomAnimal=null;
    ImageView iconAnimal=null;

    RowHolder(View row) {
        super(row);

        nomAnimal=(TextView)row.findViewById(R.id.lineName);
        iconAnimal=(ImageView)row.findViewById(R.id.icon);
        row.setOnClickListener(this);
    }

    @Override
    public void onClick(View v){
        // Do something in response to the click
        final String item = nomAnimal.getText().toString();
        Intent myIntent = new Intent(v.getContext(), AnimalActivity.class);
        myIntent.putExtra("animalName", item);
        v.getContext().startActivity(myIntent);
    }

    void bindModel(String item, int ressourceId) {
        nomAnimal.setText(item);
        iconAnimal.setImageResource(ressourceId);
    }
}