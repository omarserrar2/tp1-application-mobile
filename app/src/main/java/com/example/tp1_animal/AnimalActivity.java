package com.example.tp1_animal;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class AnimalActivity extends AppCompatActivity {
    private Animal animal = null;
    private String name = null;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.animal_info);
        name = getIntent().getStringExtra("animalName");
        TextView titre = (TextView ) this.findViewById(R.id.titre);
        ImageView image = this.findViewById(R.id.imageView2);
        final Animal animal = AnimalList.getAnimal(name);
        final TextView hightestLifespanText =(TextView ) this.findViewById(R.id.hightestLifespan);
       final TextView gestationPeriodText =(TextView ) this.findViewById(R.id.gestationPeriod);
        final TextView adultWeightText = (TextView )this.findViewById(R.id.adultWeight);
        final TextView birthWeightText = (TextView )this.findViewById(R.id.birthWeight);
        final EditText conservationStatusText = this.findViewById(R.id.conservationStatus);

        hightestLifespanText.setText(animal.getStrHightestLifespan());
        gestationPeriodText.setText(""+animal.getStrGestationPeriod());
        adultWeightText.setText(""+animal.getStrAdultWeight());
        birthWeightText.setText(""+animal.getStrBirthWeight());
        conservationStatusText.setText(animal.getConservationStatus());
        Context context = getApplicationContext();


        String picName = animal.getImgFile();
        int resourceId = context.getResources().getIdentifier(picName, "drawable", context.getPackageName());
        image.setImageResource(resourceId);
        Button save = findViewById(R.id.save);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    String conservationStatus = conservationStatusText.getText().toString();
                    animal.setConservationStatus(conservationStatus);
                    Toast.makeText(AnimalActivity.this, "Enregistré ", Toast.LENGTH_LONG).show();

                }
                catch(Exception e){
                    Toast.makeText(AnimalActivity.this, "Error ", Toast.LENGTH_LONG).show();
                }
            }
        });
        titre.setText(name);
    }
}
