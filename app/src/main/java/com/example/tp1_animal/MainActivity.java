package com.example.tp1_animal;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public final String[] items = AnimalList.getNameArray();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // Declaration de la liste des animaux
        final RecyclerView animalListe = (RecyclerView) findViewById(R.id.liste2);


        animalListe.setLayoutManager(new LinearLayoutManager(this));
        animalListe.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        // Attribuer l'IconicAdapter à la liste
        animalListe.setAdapter(new IconicAdapter());

    }

    class IconicAdapter extends RecyclerView.Adapter<RowHolder> {
        @Override
        public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return (new RowHolder(getLayoutInflater()
                    .inflate(R.layout.animal_recycler_view, parent, false)));
        }

        @Override
        public void onBindViewHolder(RowHolder holder, int position) {
           Animal animal = AnimalList.getAnimal(items[position]);
            Context context = getApplicationContext();
            int resourceId = context.getResources().getIdentifier(animal.getImgFile(), "drawable", context.getPackageName());
            try {
                holder.bindModel(items[position], resourceId);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return (items.length);
        }
    }
}
